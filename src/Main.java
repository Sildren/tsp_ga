import java.util.*;

public class Main {
    public static void main(String[] args) {
        int numberOfCities = 5;

        // generate cities matrix
        int[][] travelPrices = new int[numberOfCities][numberOfCities];
        for(int i = 0; i<numberOfCities; i++){
            for(int j=0; j<=i; j++){
                Random rand = new Random();
                if(i==j)
                    travelPrices[i][j] = 0;
                else {
                    travelPrices[i][j] = rand.nextInt(100);
                    travelPrices[j][i] = travelPrices[i][j];
                }
            }
        }

        printTravelPrices(travelPrices,numberOfCities);
        TravelingSalesman geneticAlgorithmRoulette = new TravelingSalesman(numberOfCities, SelectionTypeEnum.ROULETTE, travelPrices, 0, 0);

        TravelingSalesman geneticAlgorithmTournament = new TravelingSalesman(numberOfCities, SelectionTypeEnum.TOURNAMENT, travelPrices, 0, 0);
        Genome resultR = geneticAlgorithmRoulette.optimize();
        Genome resultT = geneticAlgorithmTournament.optimize();

        System.out.println("For roulette selection: \n" + resultR);
        System.out.println("For tournament selection: \n" + resultT);

    }

    public static void printTravelPrices(int[][] travelPrices, int numberOfCities){
        for(int i = 0; i<numberOfCities; i++){
            for(int j=0; j<numberOfCities; j++){
                System.out.print(travelPrices[i][j]);
                if(travelPrices[i][j]/10 == 0)
                    System.out.print("  ");
                else
                    System.out.print(' ');
            }
            System.out.println("");
        }
    }
}